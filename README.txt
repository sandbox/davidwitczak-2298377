CONTENTS OF THIS FILE
---------------------

 * DESCRIPTION
 * INSTALLATION
 * MAINTAINERS


DESCRIPTION
-----------

  Add HTML5 form fields to your modules
  (email, color, number, tel, url, search, output).


INSTALLATION
------------

  Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


MAINTAINERS
-----------

  David Witczak https://www.drupal.org/u/david-witczak
