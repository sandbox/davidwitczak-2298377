<?php
/**
 * @file
 * Theme for the html5 form field module.
 *
 * Contains the theme functions for all the elements module html5 form field.
 */

/**
 * Returns HTML for a email form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size, #maxlength,
 *   #required, #attributes, #autocomplete_path, #placeholder, #disabled.
 */
function theme_email(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'email';

  html5_form_field_attributes($element);

  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'placeholder',
  ));

  _form_set_class($element, array('form-text', 'form-email'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}

/**
 * Returns HTML for a tel form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size, #maxlength,
 *   #required, #attributes, #placeholder, #disabled, #pattern.
 */
function theme_tel(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'tel';

  html5_form_field_attributes($element);

  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
    'placeholder',
  ));

  _form_set_class($element, array('form-text', 'form-tel'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}

/**
 * Returns HTML for a url form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size, #maxlength,
 *   #required, #attributes, #placeholder, #disabled.
 */
function theme_url(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'url';

  html5_form_field_attributes($element);

  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
    'placeholder',
  ));

  _form_set_class($element, array('form-text', 'form-url'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}

/**
 * Returns HTML for a search form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size, #maxlength,
 *   #required, #attributes, #autocomplete_path, #placeholder, #disabled.
 */
function theme_search(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'search';

  html5_form_field_attributes($element);

  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
    'placeholder',
  ));

  _form_set_class($element, array('form-text', 'form-search'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}

/**
 * Returns HTML for a color form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size,
 *   #attributes, #disabled.
 */
function theme_color(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'color';

  html5_form_field_attributes($element);

  element_set_attributes($element, array('id', 'name', 'value'));
  _form_set_class($element, array('form-color'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}

/**
 * Returns HTML for a number form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #size, #maxlength,
 *   #required, #attributes, #placeholder, #disabled, #step, #min, #max.
 */
function theme_number(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'number';

  html5_form_field_attributes($element);

  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'maxlength',
    'placeholder',
    'step',
    'min',
    'max',
  ));

  _form_set_class($element, array('form-text', 'form-number'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}

/**
 * Returns HTML for a range form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #description, #attributes, #placeholder,
 *   #disabled, #step, #min, #max.
 */
function theme_range(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'range';

  html5_form_field_attributes($element);

  element_set_attributes($element, array('id', 'name', 'step', 'min', 'max'));
  _form_set_class($element, array('form-range'));

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output;
}

/**
 * Returns HTML for a output form element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *   Properties used: #title, #value, #description, #attributes.
 *
 * @ingroup themeable
 */
function theme_output(array $variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'output';

  element_set_attributes($element, array('id', 'name', 'for', 'form'));
  _form_set_class($element, array('form-output'));

  $wrapper_attributes = array(
    'class' => array('form-output-wrapper'),
  );

  $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
  $output .= '<output' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</output>';
  $output .= '</div>';
  return $output;
}

/**
 * Adds html5 attributes in array #attributes element.
 *
 * @param array $element
 *   An associative array.
 */
function html5_form_field_attributes(array &$element) {
  $element_types = array('search', 'url', 'tel', 'email', 'number');

  if (in_array($element['#type'], $element_types) && $element['#required']) {
    $element['#attributes']['required'] = 'required';
  }

  if (isset($element['#autofocus'])) {
    $element['#attributes']['autofocus'] = 'autofocus';
  }

  if ($element['#type'] == 'output' && $element['#for']) {
    $for_output = '';
    foreach ($element['#for'] as $value) {
      $for_output .= ' edit-' . $value;
    }
    $element['#attributes']['for'] = trim($for_output);
  }
}
