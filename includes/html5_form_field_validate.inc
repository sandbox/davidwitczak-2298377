<?php
/**
 * @file
 * Validate for the html5 form field module.
 *
 * Contains the validate functions for all the elements module html5 form field.
 */

/**
 * Form element validation function email.
 */
function html5_form_field_validate_email(&$element, &$form_state) {
  if ($element['#value'] && !valid_email_address($element['#value'])) {
    form_error($element, t('The field %title is not valid.', array('%title' => $element['#title'])));
  }
}

/**
 * Form element validation function tel.
 */
function html5_form_field_validate_tel(&$element, &$form_state) {
  if ($element['#value'] !== '') {
    $pattern = "#(\+\d+(\s|-))?0\d(\s|-)?(\d{2}(\s|-)?){4}#";

    if (!preg_match($pattern, $element['#value'])) {
      form_error($element, t('The field %title is not in the right format.', array('%title' => $element['#title'])));
    }
  }
}

/**
 * Form element validation function url.
 */
function html5_form_field_validate_url(&$element, &$form_state) {
  if ($element['#value'] != '' && !valid_url($element['#value'], TRUE)) {
    form_error($element, t('The field %title is not valid.', array('%title' => $element['#title'])));
  }
}

/**
 * Form element validation function color.
 */
function html5_form_field_validate_color(&$element, &$form_state) {
  if ($element['#value']) {
    $pattern = "/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/";

    if (!preg_match($pattern, $element['#value'])) {
      form_error($element, t('The field %title is not in the right format.', array('%title' => $element['#title'])));
    }
  }
}

/**
 * Form element validation function number.
 */
function html5_form_field_validate_number(&$element, &$form_state) {
  if ($element['#value']) {
    if (!is_numeric($element['#value'])) {
      form_error($element, t('The field %title is not a number.', array('%title' => $element['#title'])));
    }
    elseif (isset($element['#min']) && $element['#value'] < $element['#min']) {
      form_error($element, t('The field %title is less than the minimum required.', array('%title' => $element['#title'])));
    }
    elseif (isset($element['#max']) && $element['#value'] > $element['#max']) {
      form_error($element, t('The field %title is greater than the maximum required.', array('%title' => $element['#title'])));
    }
  }
}

/**
 * Form element validation function range.
 */
function html5_form_field_validate_range(&$element, &$form_state) {
  if (isset($element['#min']) && $element['#value'] < $element['#min']) {
    form_error($element, t('The field %title is less than the minimum required.', array('%title' => $element['#title'])));
  }
  elseif (isset($element['#max']) && $element['#value'] > $element['#max']) {
    form_error($element, t('The field %title is greater than the maximum required.', array('%title' => $element['#title'])));
  }
}
